import React from "react";
import { useTranslation } from "react-i18next";

import "./HomePage.scss";

const HomePage = () => {
  const [t] = useTranslation("global");

  return (
    <section>
      <span className="span">
      </span>

      <div className="home">
        <span className="home__img"></span>
        <br></br>
      </div>
      <p className="text">{t("aboutme.aboutme-short")}</p>
    </section>
  );
};

export default HomePage;
