import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faLinkedin,
  faGitlab,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";

import "./Footer.scss";

const Footer = () => {
  return (
    <>
      <div className="footer">
        <a
          className="footer__icon1"
          href="https://gitlab.com/dashboard/projects?personal=true&sort=latest_activity_desc"
        >
          <FontAwesomeIcon icon={faGitlab} />
        </a>

        <a
          className="footer__icon2"
          href="https://www.linkedin.com/in/oscar-de-la-fuente-toledo/"
        >
          <FontAwesomeIcon icon={faLinkedin} />
        </a>

        <a
          className="footer__icon3"
          href="https://www.linkedin.com/in/oscar-de-la-fuente-toledo/"
        >
          <FontAwesomeIcon icon={faInstagram} />
        </a>
      </div>

      <div className="footer__icon4">
        <p>© 2021 Oscar de la Fuente Toledo</p>
      </div>
    </>
  );
};

export default Footer;
