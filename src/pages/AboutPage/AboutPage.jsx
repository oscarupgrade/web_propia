import React, { useState } from "react";

import {useTranslation} from "react-i18next";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowDown, faArrowUp } from "@fortawesome/free-solid-svg-icons";

import "./AboutPage.scss";

const AboutPage = () => {

  const [t] = useTranslation("global");
  const [about, setAbout] = useState(false);

  const showAbout = () => {
    setAbout(!about);
  };

  return (
    <section className="body" id="about">
      <div onClick={showAbout} className={about ? "about active" : "about"}>
        <h2 className="about__text--size">{t("aboutme.aboutme-title")}</h2>

        <FontAwesomeIcon
          className="about__icon"
          icon={about ? faArrowUp : faArrowDown}
        />
        <br></br>
        <p className={about ? "about__text" : "about__text active"}>
          {t("aboutme.aboutme-large")}
          <br></br>
          {t("aboutme.aboutme-secondlarge")}
        </p>
      </div>
    </section>
  );
};

export default AboutPage;
