import React from "react";

import {useTranslation} from "react-i18next";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { faLinkedin, faGitlab } from "@fortawesome/free-brands-svg-icons";

import "./ContactPage.scss";

const ContactPage = () => {

  const [t] = useTranslation("global");

  return (
    <section className="padding" id="contact">
      <div className="contact">
        <h2>{t("contact.contactme")}</h2>

        <FontAwesomeIcon className="contact__iconEmail" icon={faEnvelope} />
        <p className="contact__text1">oscardelafuentetoledo10@gmail.com</p>

        <div className="contact__finish">
          <a href="https://www.linkedin.com/in/oscar-de-la-fuente-toledo/">
            <FontAwesomeIcon
              className="contact__iconLinkedin"
              icon={faLinkedin}
            />
          </a>

          <a
            className="contact__gitlab"
            href="https://gitlab.com/dashboard/projects?personal=true&sort=latest_activity_desc"
          >
            <FontAwesomeIcon icon={faGitlab} />
          </a>
        </div>
        <p>© 2021 Oscar de la Fuente Toledo</p>
      </div>
    </section>
  );
};

export default ContactPage;
