import React, { useState } from "react";

import { useTranslation } from "react-i18next";
import { Link, animateScroll as scroll } from "react-scroll";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBars,
  faTimes,
  faHome,
  faCode,
  faProjectDiagram,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { faIdBadge } from "@fortawesome/free-regular-svg-icons";

import "./Header.scss";

const Header = () => {
  const [t, i18n] = useTranslation("global");
  const [header, setHeader] = useState(false);

  const showNavbar = () => {
    setHeader(!header);
  };

  const scrollToTop = () => {
    scroll.scrollToTop();
  };

  return (
    <nav className="header">
      <p className="header__link">OFT</p>

      <div className="header__title">
        <h1>Full Stack Developer</h1>
      </div>

      <div className="header__icon">
        <FontAwesomeIcon
          onClick={showNavbar}
          className="header__icon--position"
          icon={faBars}
        />
      </div>

      <div className={header ? "header__menu active" : "header__menu"}>
        <FontAwesomeIcon
          onClick={showNavbar}
          className="header__menu__icon"
          icon={faTimes}
        />

        <ul onClick={showNavbar}>
          <li className='brands--position'>
            <img
              onClick={() => i18n.changeLanguage("es")}
              className="brands"
              src="/img/espana.png"
              alt="imagen bandera españa"
            />

            <img
              onClick={() => i18n.changeLanguage("en")}
              className="brands"
              src="/img/reinounido.png"
              alt="imagen bandera reino unido"
            />
          </li>
          <li
            onClick={scrollToTop}
            className="header__menu__link header__menu__link--position "
          >
            <FontAwesomeIcon className="header__icon-menu" icon={faHome} />
            {t("menu.menu-initiation")}
          </li>

          <Link onClick={showNavbar} to="about" offset={-150}>
            <li className="header__menu__link">
              <FontAwesomeIcon className="header__icon-menu" icon={faUser} />
              {t("menu.menu-aboutme")}
            </li>
          </Link>

          <Link onClick={showNavbar} to="code" offset={-100}>
            <li className="header__menu__link">
              <FontAwesomeIcon className="header__icon-menu" icon={faCode} />
              {t("menu.menu-mycode")}
            </li>
          </Link>

          <Link onClick={showNavbar} to="proyects" offset={-80}>
            <li className="header__menu__link">
              <FontAwesomeIcon
                className="header__icon-menu"
                icon={faProjectDiagram}
              />
              {t("menu.menu-projects")}
            </li>
          </Link>

          <Link onClick={showNavbar} to="contact">
            <li className="header__menu__link">
              <FontAwesomeIcon className="header__icon-menu" icon={faIdBadge} />
              {t("menu.menu-contact")}
            </li>
          </Link>
        </ul>
      </div>
    </nav>
  );
};

export default Header;
