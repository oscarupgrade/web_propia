import React, { useState } from "react";

import { useTranslation } from "react-i18next";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGitlab } from "@fortawesome/free-brands-svg-icons";
import { faEye } from "@fortawesome/free-regular-svg-icons";
import { faTimes, faDoorOpen } from "@fortawesome/free-solid-svg-icons";

import "./ProjectsPage.scss";

const ProjectsPage = () => {
  const [t] = useTranslation("global");

  const [calculator, setCalculator] = useState(false);
  const [weather, setWeather] = useState(false);
  const [recipes, setRecipes] = useState(false);
  const [todoList, setTodoList] = useState(false);

  const showCalculator = () => {
    setCalculator(!calculator);
  };

  const showWeather = () => {
    setWeather(!weather);
  };

  const showRecipes = () => {
    setRecipes(!recipes);
  };

  const showTodoList = () => {
    setTodoList(!todoList);
  };

  return (
    <section className="projects" id="proyects">
      <h1 className="projects__title1">{t("projects.projects-title1")}</h1>

      {calculator ? (
        <div className="projects__second-menu">
          <FontAwesomeIcon
            onClick={showCalculator}
            className="projects__second-menu__icon"
            icon={faTimes}
          />

          <p className="projects__text">
            {t("projects.projects-calculatortext")}
          </p>

          <div>
            <a href="https://apple-calculator.netlify.app/">
              <FontAwesomeIcon icon={faEye} />
            </a>
            <a href="https://gitlab.com/oscarupgrade/calculator_javascript">
              <FontAwesomeIcon icon={faGitlab} />
            </a>
          </div>
        </div>
      ) : (
        <div className="projects__first-menu--size">
          <h2 className="projects__name1 ">
            {t("projects.projects-calculator")}
          </h2>
          <FontAwesomeIcon
            onClick={showCalculator}
            className="projects__first-menu__icon"
            icon={faDoorOpen}
          />
        </div>
      )}

      {weather ? (
        <div className="projects__second-menu">
          <FontAwesomeIcon
            onClick={showWeather}
            className="projects__second-menu__icon"
            icon={faTimes}
          />

          <p className="projects__text">{t("projects.projects-weathertext")}</p>

          <div>
            <a href="https://wheatherworld2.netlify.app/">
              <FontAwesomeIcon className="projects__icon" icon={faEye} />
            </a>
            <a href="https://gitlab.com/oscarupgrade/weather_world_react">
              <FontAwesomeIcon icon={faGitlab} />
            </a>
          </div>
        </div>
      ) : (
        <div className="projects__first-menu--size">
          <h2 className="projects__name1 ">Weather</h2>
          <FontAwesomeIcon
            onClick={showWeather}
            className="projects__first-menu__icon"
            icon={faDoorOpen}
          />
        </div>
      )}

      {recipes ? (
        <div className="projects__second-menu">
          <FontAwesomeIcon
            onClick={showRecipes}
            className="projects__second-menu__icon"
            icon={faTimes}
          />

          <p className="projects__text">{t("projects.projects-recipestext")}</p>

          <div>
            <a href="https://recipesworld.netlify.app/">
              <FontAwesomeIcon icon={faEye} />
            </a>
            <a href="https://gitlab.com/oscarupgrade/recipe_react">
              <FontAwesomeIcon icon={faGitlab} />
            </a>
          </div>
        </div>
      ) : (
        <div className="projects__first-menu--size">
          <h2 className="projects__name1 ">Recipes</h2>
          <FontAwesomeIcon
            onClick={showRecipes}
            className="projects__first-menu__icon"
            icon={faDoorOpen}
          />
        </div>
      )}

      {todoList ? (
        <div className="projects__second-menu">
          <FontAwesomeIcon
            onClick={showTodoList}
            className="projects__second-menu__icon"
            icon={faTimes}
          />

          <p className="projects__text">
            {t("projects.projects-todolisttext")}
          </p>

          <div>
            <a href="https://listthings.netlify.app/">
              <FontAwesomeIcon icon={faEye} />
            </a>
            <a href="https://gitlab.com/oscarupgrade/todolist_react">
              <FontAwesomeIcon icon={faGitlab} />
            </a>
          </div>
        </div>
      ) : (
        <div className="projects__first-menu--size">
          <h2 className="projects__name1 ">Todo List</h2>
          <FontAwesomeIcon
            onClick={showTodoList}
            className="projects__first-menu__icon"
            icon={faDoorOpen}
          />
        </div>
      )}
    </section>
  );
};

export default ProjectsPage;
