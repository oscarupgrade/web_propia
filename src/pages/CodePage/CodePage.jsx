import React from "react";

import { useTranslation } from "react-i18next";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSitemap, faSyncAlt } from "@fortawesome/free-solid-svg-icons";
import { faFileCode } from "@fortawesome/free-regular-svg-icons";

import "./CodePage.scss";

const CodePage = () => {

  const [t] = useTranslation("global");

  return (
    <section className="body" id="code">
      <div className="code">
        <h1 className="code__title"> {t("code.code-title")} </h1>

        <div>
          <FontAwesomeIcon className="code__icon" icon={faSitemap} />
          <h2>{t("code.code-firsttitle")}</h2>
          <p className="code__text">
          {t("code.code-firsttext")}
          </p>
        </div>

        <div>
          <FontAwesomeIcon className="code__icon" icon={faFileCode} />
          <h2>{t("code.code-secondtitle")}</h2>
          <p className="code__text">
          {t("code.code-secondtext")}
          </p>
        </div>

        <div>
          <FontAwesomeIcon className="code__icon" icon={faSyncAlt} />
          <h2>{t("code.code-thirdtitle")}</h2>
          <p className="code__text">
          {t("code.code-thirdtext")}
          </p>
        </div>
      </div>
    </section>
  );
};

export default CodePage;
