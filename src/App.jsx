import React from "react";
// import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.scss";


import Header from './components/Header';

import HomePage from "./pages/HomePage";
import ProjectsPage from './pages/ProjectsPage';
import ContactPage from './pages/ContactPage';
import CodePage from './pages/CodePage';
import AboutPage from './pages/AboutPage';


function App() {
  return (
    <>
      <Header />
      <HomePage />
      <AboutPage />
      <ProjectsPage />
      <CodePage />
      <ContactPage />
    </>
  );
}

export default App;
